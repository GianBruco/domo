import { initializeApp } from "firebase/app";

// insert configuration here
const firebaseConfig = {
    apiKey: "",
    authDomain: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: "",
};

export const app = initializeApp(firebaseConfig);

export default app;