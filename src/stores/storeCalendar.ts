import { writable } from "svelte/store";
import { browser } from "$app/environment";

type Event = {
	id: number
	name: string
	time: string
}

type DailyEvents = {
	day: string
	events: Array<Event>
}

type EventList = Array<DailyEvents>;

let initialList: EventList = [{
	day: "September 17 1991",
	events: [
		{
			id: 12345,
			name: "Linux Initial Release",
			time: "00:00"
		}
	]
}];

const storedListEvent = JSON.parse(browser && localStorage.getItem("dailyEvents")) || initialList;

export const dailyEvents = writable<EventList>(browser && storedListEvent);


dailyEvents.subscribe((value) => browser && (localStorage.dailyEvents = JSON.stringify(value)));
