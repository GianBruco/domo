# Domo

Domo è una PWA creata come progetto finale per il corso di *Sviluppo applicazioni web* presso la facoltà di informatica all'Università di Pisa.

![](/Images/full.png)

Start page | Login page | Sign up page
:---:|:---:|:---:|
![](/Images/start.png) | ![](/Images/login.png) | ![](/Images/signUp.png)

Come si può vedere dalle immagini soprastanti, l'applicazione offre un servizio di autenticazione, sia tramite account Google, che tramite registrazione con email e password; entrambi i metodi si appoggiano a Firebase. All'interno l'applicazione si compone di 4 moduli:
- un orologio/cronometro;
- un calendario;
- un visualizzatore delle previsioni metereologiche;
- un visualizzatore di notizie tramite feed RSS


Nello specifico, le tecnologie usate per questo progetto sono:
- Svelte
- TailwindCSS
- Firebase
- OpenMeteo API


## Installazione

1. Scaricare la repository.
2. Eseguire il comando `npm install` all'interno della cartella appena scaricata per installare le dipendenze.
3. Creare una nuova app su firebase e inserire la configurazione nel file `src/firebaseConf.ts`.
4. Per poter eseguire correttamente alcune funzioni dell'applicazione (richeste di feed RSS) occorre aprire il sito `https://cors-anywhere.herokuapp.com/corsdemo` e sbloccare temporaneamente la demo per il browser.
5. Eseguire il comando `npm run dev` all'interno della stessa cartella per avviare l'applicazione su `http://localhost:port/`.


## Utilizzo

### Start Page
- Dalla schermata principale si può passare alle schermate di accesso e registrazione tramite gli appositi pulsanti posizionati in alto a destra.

### Orologio/Cronometro
- Questo modulo è composto sia da un orologio che da un cronometro. È possibile passare da uno all'altro tramite il pulsate con l'icona desiderata.
- Il modulo cronometro permette di avviare il tempo, di metterlo in pausa o di resettarlo allo stato originale tramite dei pulsanti che appariranno a seconda dello stato corrente.

### Calendario
- Il modulo calendario permette di passare da un mese all'altro tramite le icone con i simboli `<` `>` e di aggiungere/rimuovere un evento nel giorno desiderato semplicemente cliccandoci sopra. In questo ultimo caso si aprirà una finestra di dialogo nella quale si potranno fare due diverse cose:
    - inserire il nome e l'ora dell'evento per poi aggiungerlo alla lista degli eventi giornalieri;
    - visionare gli altri eventi già inseriti in quella data con la possibilità di rimuoverli.

### Previsioni meteo
- Il modulo delle previsioni meteo mostra i dati ricevuti da OpenMeteo tramite una richiesta alla Forecast API e li aggiorna ogni *n* minuti.
- È possibile cambiare il luogo usato per le previsioni tramite l'icona della matita in alto a destra inserendo poi i dati richiesti dalla finestra di dialogo.

### Visualizzatore notizie
- In questo modulo vengono mostrati a rotazione i titoli degli ultimi articoli presenti nei feed RSS inseriti dall'utente. Ogni *n* secondi verrà mostrato l'articolo successivo (in ordine dal più recente al meno recente) ma si può forzare questa attesa utilizzando i tasti `<` `>`.
- Anche in questo caso l'icona della matita serve per aggiungere un nuovo feed Rss tramite l'inserimento dei dati richiesti oppure visualizzare i feed già inseriti ed eventualmente rimuoverli.

- Alcuni esempi di feed RSS
    - HDBlog: https://www.hdblog.it/rss
    - Wired: https://www.wired.it/feed/rss
    - AstroSpace: https://www.astrospace.it/feed
    - Tegamini.it: https://www.tegamini.it/feed