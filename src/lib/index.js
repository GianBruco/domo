import { dev } from '$app/environment';

export function requestNotification(title, text) {
    if ("Notification" in window) {
        //console.log("Notification supported");
        if (Notification.permission === "granted") {
            notify(title, text);
        } else {
            Notification.requestPermission().then((res) => {
                if (res === "granted") {
                    notify(title, text);
                } else if (res === "denied") {
                    console.log("Notification access denied");
                } else if (res === "default") {
                    console.log("Notification permission not given");
                }
            });
        }
    } else {
        console.error("Notification Not Suppoerted");
    }
}

function notify(title, text) {
    if (text !== "") {
        new Notification(title),
        {
            body: text,
            lang: "it",
            icon: "/logo_192.png",
            vibrate: [200, 100, 200],
        };
    }
    else {
        new Notification(title);
    }
}