import { writable } from "svelte/store";
import { browser } from "$app/environment";

interface Location {
  nameLocation: string
  latitude: number
  longitude: number
}

let initialLocation: Location = {
  nameLocation: "Roma",
  latitude: 41.89,
  longitude: 12.4827
}

const storedLocation = JSON.parse(browser && localStorage.getItem("location")) || initialLocation;

export const location = writable<Location>(browser && storedLocation);


location.subscribe((value) => browser && (localStorage.location = JSON.stringify(value)));
