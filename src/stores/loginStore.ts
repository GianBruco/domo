import type { User } from 'firebase/auth';
import { getAuth, GoogleAuthProvider, onAuthStateChanged, signInWithPopup, signInWithEmailAndPassword, signOut, createUserWithEmailAndPassword } from "firebase/auth";
import { writable } from "svelte/store";
import { app } from '../firebaseConf';
import { requestNotification } from "../lib/index.js";

const auth = getAuth(app);
const providerGoogle = new GoogleAuthProvider();

function createUserStore() {
    const { subscribe, set, update } = writable<User>();

    // Login with email and password
    async function login(email: string, pwd: string) {
        try {
            return await signInWithEmailAndPassword(auth, email, pwd);
        } catch (err) {
            requestNotification(
                "Autenticazione fallita",
                "Email o password non corrette!",
            );
            return false;
        }
    }

    // login with google
    function googleLogin() {
        signInWithPopup(auth, providerGoogle);
    }

    // sign up with email and password 
    async function signUpEmail(email: string, pwd: string) {
        try {
            createUserWithEmailAndPassword(auth, email, pwd);
        } catch (err) {
            requestNotification(
                "Creazione utente fallita",
                err,
            );
        }
    }

    // change state of user (login/logout)
    onAuthStateChanged(auth, (user) => {
        if (user) {
            // signed in
            userStore.set(user);
        } else {
            // signed out
            userStore.set(undefined);
        }
    });

    // logout user
    async function logout() {
        await signOut(auth);
    }

    return {
        subscribe, set, update, login, googleLogin, logout, signUpEmail,
    };
}

export const userStore = createUserStore(); 