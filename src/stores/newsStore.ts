import { writable } from "svelte/store";
import { browser } from "$app/environment";


interface NewsUrl {
    id: number
    name: string
    url: string
}


let initialNews: NewsUrl[] = [{
    id: 0,
    name: "Hd Blog",
    url: "https://www.hdblog.it/rss/"
}]

const storedNews = JSON.parse(browser && localStorage.getItem("news")) || initialNews;

export const newsUrl = writable<NewsUrl>(browser && storedNews);


newsUrl.subscribe((value) => browser && (localStorage.news = JSON.stringify(value)));
